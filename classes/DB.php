<?php
/**
 * User: Evgeniy Kalenov
 * Date: 16.02.2018
 */

use service\Utility;

abstract class DB extends SQLite3
{
    const DB1_NAME = 'sync_db1';
    const DB2_NAME = 'sync_db2';
    const UNLOAD_TABLE = 'ObjectUnload';

    const UNLOAD_EMPLOYEE_TYPE = 'EMPLOYEE';
    const UNLOAD_OUTLET_TYPE = 'OUTLET';
    const UNLOAD_SKU_TYPE = 'SKU';

    protected $db_name;

    private static $unload_table_fields = [                                 // структура таблицы для выгрузки синхронизируемых объектов
        'type',             // string
        'id',               // integer
        'state_1',          // string | null
        'state_2',          // string | null
        'state_3',          // string | null
        'relation_id'       // integer | null
    ];

    private static $unload_objects = ['Outlet'];         // перечень синхронизируемых объектов

    /*
     * Создает файлы баз данных
     */
    public static function init()
    {
        $databases = [self::DB1_NAME, self::DB2_NAME];
        foreach ($databases as $base){
            $db_file = DB_PATH . "/$base.db";
            if(!file_exists($db_file)){
                try{
                    $db = new SQLite3($db_file);
                    $query_file = file_get_contents(FILES_PATH . "\\" . $base . "_init_query.txt");
                    if(!empty($query_file)){
                        if(!$db->exec($query_file)){
                            new Exception("Ошибка иницилизации баз данных");
                            break;
                        }
                    }
                }catch (Exception $e){
                    die ($e->getMessage());
                }
            }
        }
    }

    /*
     * Устанавливает соединение с базой данных
     *
     * @param string
     */
    public function __construct()
    {
        $this->open(DB_PATH . "/$this->db_name.db");
    }
    
    public function getDbName()
    {
        return $this->db_name;
    }

    /*
     * Возвращает объект используемый в данной БД
     *
     * @param string
     * @return object
     */
    protected function getModel($className)
    {
        if(class_exists($className)){
            if($className == 'Employee'){
                return new Employee();
            }
            elseif ($this->db_name == DB::DB1_NAME){
                $obj = '\\db1\\' . $className;
                return new $obj;
            }
            elseif ($this->db_name == DB::DB2_NAME){
                $obj = '\\db2\\' . $className;
                return new $obj;
            }
        }
        throw new Exception("Не определен объект для выгрузки");
    }

    /*
     * @param object
     */
    private function getModelUploadType($model)
    {
        switch ($model){
            case ($model instanceof Employee):
                return self::UNLOAD_EMPLOYEE_TYPE;
                break;
            case ($model instanceof Outlet):
                return self::UNLOAD_OUTLET_TYPE;
                break;
            case ($model instanceof Sku):
                return self::UNLOAD_SKU_TYPE;
                break;
            default:
                throw new Exception;
        }
    }

    /*
     * Возвращает данные для выгрузки в таблицу self::UNLOAD_TABLE
     *
     * @param string
     */
    private function getUploadObjectsState($select)
    {
        if(!empty($select) && is_string($select))
        {
            Utility::debug($select);
            $rows = $this->query($select);
            $res = [];
            while($row = $rows->fetchArray(SQLITE3_ASSOC)){
                if(!isset($row)){
                    continue;
                }
                $res[] = $row;
            }
            return $res;
        }
        return [];
    }

    /*
     * Синхронизирует базу данных
     */
    public function synchronize()
    {
        foreach (self::$unload_objects as $className)
        {
            $model = $this->getModel($className);                       // получили модель выгружаемого объекта
            $select = $model->getUploadSelect();                        // свормировали select для получения записей из таблиицы объекта
            $upload_data = $this->getUploadObjectsState($select);       // получили массив объектов для выгрузки

            $type = $this->getModelUploadType($model);                  // тип объекта для записи в таблицу ObjectUnload

            /* TODO */
        }

        return;
    }
}