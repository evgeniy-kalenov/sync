<?php
/**
 * User: Evgeniy Kalenov
 * Date: 16.02.2018
 */

class Employee extends Model
{
    const TABLE = 'Employee';

    public $id;
    public $createAt;
    public $modifiedAt;
    public $name;

    public function __construct(){
        $this->createAt = date("Y-m-d H:i:s");
    }
    
    public function getTable(){
        return self::TABLE;
    }

    public function getPropertiesToUpload()
    {
        return [
            'id',
            'createAt',
            'modifiedAt',
            'name'
        ];
    }
    
}