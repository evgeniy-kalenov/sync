<?php
/**
 * User: Evgeniy Kalenov
 * Date: 16.02.2018
 */

abstract class Model
{
    /*
     * Возвращает имя таблицы в БД
     * @return string
     */
    abstract protected function getTable();
    
    /*
     * Возвращает свойства объекта (без значений)
     * Формат: [0] => id
     * @return array
     */
    public function getProperties()
    {
        $res = [];
        foreach (get_object_vars($this) as $key => $value){
            $res[] = $key;
        }
        return $res;
    }

    /*
     * Возвращает свойства объекта (без значений)
     * Формат: [0] => :id
     * @return array
     */
    public function getBindProperties()
    {
        $res = [];
        foreach ($this->getProperties() as $prop){
            $res[] = ':'.$prop;
        }
        return $res;
    }

    /*
     * Возвращает подготовленные параметры
     * Формат: [:createAt] => 2018-02-16 20:14:51
     * @return array
     */
    public function getBindParams()
    {
        $res = [];
        foreach (get_object_vars($this) as $prop => $value){
            $res[':'.$prop] = $value;
        }
        return $res;
    }

    /*
     * Возвращает подготовленную строку параметров для UPDATE (все кроме id)
     * Формат: modifiedAt = :modifiedAt, name = :name
     * @return string
     */
    public function getBindParamsForUpdate()
    {
        $res = [];
        
        $params = $this->getProperties();
        foreach ($params as $param) {
            if($param == 'id'){
                continue;
            }
            $res[] = "$param = :$param";
        }
        return implode(', ', $res);
    }

    /*
     * Возвращает свойства объекта со значениями
     * @return array
     */
    public function getAttributes()
    {
        return get_object_vars($this);
    }
    
    /*
     * Заполняет свойства объекта значениями
     * @param array $data
     * @return void
     */
    public function setAttributes(Array $data)
    {
        $attributes = $this->getAttributes();
        foreach ($attributes as $attr => $value){
            if(array_key_exists($attr, $data)){
                $this->$attr = $data[$attr];
            }
        }
        return;
    }

    /*
    * Возвращает выгружаемые свойства себя и связного объекта для формирования sql-запроса
    * Формат: свойство => алиас
    * 
    * @return array | null
    */
    abstract public function getPropertiesToUpload();
}