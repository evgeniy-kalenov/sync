<?php
/**
 * Created by PhpStorm.
 * User: Kalenov Evgeniy
 * Date: 17.02.2018
 */

abstract class Outlet extends Model
{
    public $id;
    public $createAt;
    public $modifiedAt;
    public $name;
    
    public function __construct(){
        $this->createAt = date("Y-m-d H:i:s");
    }
}