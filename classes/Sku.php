<?php
/**
 * User: Evgeniy Kalenov
 * Date: 19.02.2018
 */


abstract class Sku extends Model
{
    public $id;
    public $createAt;
    public $modifiedAt;
    public $name;

    public function __construct(){
        $this->createAt = date("Y-m-d H:i:s");
    }
}