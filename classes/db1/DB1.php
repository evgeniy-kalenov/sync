<?php

/**
 * User: Evgeniy Kalenov
 * Date: 22.02.2018
 */

namespace db1;

use DB;

class DB1 extends DB
{
    protected $db_name = DB::DB1_NAME;

    protected $sync_fields = [
        'Employee' => ['id', 'name', 'createAt', 'modifiedAt'],
        'Outlet' => ['id', 'name', 'createAt', 'modifiedAt'],
        'Owner' => ['id', 'name', 'createAt', 'modifiedAt'],
        'Sku' => ['id', 'name', 'stock', 'createAt', 'modifiedAt']
    ];
}