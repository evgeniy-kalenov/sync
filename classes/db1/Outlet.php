<?php
/**
 * Created by PhpStorm.
 * User: Kalenov Evgeniy
 * Date: 17.02.2018
 */

namespace db1;

use service\Utility;

class Outlet extends \Outlet
{
    const TABLE = 'Outlet';
    
    public $owner_id;
    
    public function getTable(){
        return self::TABLE;
    }

    public function getPropertiesToUpload()
    {
        return [
            'Outlet.id AS outlet_id',
            'Outlet.createAt AS outlet_createAt',
            'Outlet.modifiedAt AS outlet_modifiedAt',
            'Outlet.name AS outlet_name',
            'Owner.name AS owner_name',
            'Owner.createAt AS owner_createAt',
            'Owner.modifiedAt AS owner_modifiedAt'
        ];

    }

    public function getUploadSelect()
    {
        $fields = implode(', ', $this->getPropertiesToUpload());
        $table = $this->getTable();

        $select = "SELECT $fields FROM $table LEFT JOIN Owner ON $table.owner_id = Owner.id";
        
        return $select;
    }
}