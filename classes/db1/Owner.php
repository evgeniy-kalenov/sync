<?php
/**
 * User: Evgeniy Kalenov
 * Date: 19.02.2018
 */


namespace db1;

use Model;

class Owner extends Model
{
    const TABLE = 'Owner';

    public $id;
    public $createAt;
    public $modifiedAt;
    public $name;

    public function __construct(){
        $this->createAt = date("Y-m-d H:i:s");
    }

    public function getTable(){
        return self::TABLE;
    }
}