<?php
/**
 * User: Evgeniy Kalenov
 * Date: 19.02.2018
 */

namespace db1;

class Sku extends \Sku
{
    const TABLE = 'Sku';
    
    public $stock;

    public function getTable(){
        return self::TABLE;
    }
}