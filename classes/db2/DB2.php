<?php

/**
 * User: Evgeniy Kalenov
 * Date: 22.02.2018
 */

namespace db2;

use DB;

class DB2 extends DB
{
    protected $db_name = DB::DB2_NAME;

    private $sync_fields = [
        'Employee' => ['name', 'modifiedAt'],
        'Outlet' => ['name', 'owner_name', 'modifiedAt'],
        'Sku' => ['name', 'modifiedAt'],
        'SkuStock' => ['stock', 'modifiedAt']
    ];
}