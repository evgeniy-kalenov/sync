<?php
/**
 * Created by PhpStorm.
 * User: Kalenov Evgeniy
 * Date: 17.02.2018
 */

namespace db2;

class Outlet extends \Outlet
{
    const TABLE = 'Outlet';
    
    public $owner_name;

    public function getTable(){
        return self::TABLE;
    }

    public function getPropertiesToUpload()
    {
        return [
            'Outlet.id',
            'Outlet.createAt',
            'Outlet.modifiedAt',
            'Outlet.name',
            'Outlet.name AS owner_name',
            'Outlet.createAt AS owner_createAt',
            'Outlet.modifiedAt AS owner_modifiedAt'
        ];
    }
}