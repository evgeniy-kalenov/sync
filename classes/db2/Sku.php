<?php
/**
 * User: Evgeniy Kalenov
 * Date: 19.02.2018
 */

namespace db2;

use Model;

class Sku extends \Sku
{
    const TABLE = 'Sku';

    public function getTable(){
        return self::TABLE;
    }
}