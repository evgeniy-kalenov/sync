<?php
/**
 * User: Evgeniy Kalenov
 * Date: 19.02.2018
 */

namespace db2;

class SkuStock extends \Model
{
    const TABLE = 'SkuStock';

    public $id;
    public $createAt;
    public $modifiedAt;
    public $sku_id;
    public $stock;

    public function __construct(){
        $this->createAt = date("Y-m-d H:i:s");
    }

    public function getTable(){
        return self::TABLE;
    }
}