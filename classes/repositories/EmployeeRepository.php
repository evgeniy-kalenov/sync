<?php
/**
 * User: Evgeniy Kalenov
 * Date: 16.02.2018
 */

namespace repositories;

use DB;
use Employee;

class EmployeeRepository extends Repository
{
    public function getModel()
    {
        return new Employee();
    }
}