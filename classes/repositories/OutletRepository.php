<?php
/**
 * Created by PhpStorm.
 * User: Kalenov Evgeniy
 * Date: 17.02.2018
 */

namespace repositories;

use DB;

class OutletRepository extends Repository
{
    public function getModel()
    {
        switch (self::$db_name){
            case DB::DB1_NAME:
                return new \db1\Outlet();
                break;
            case DB::DB2_NAME:
                return new \db2\Outlet();
                break;
            default:
                throw new \Exception("Не определен объект репозитория");
        }
    }
}