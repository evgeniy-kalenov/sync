<?php
/**
 * Created by PhpStorm.
 * User: Kalenov Evgeniy
 * Date: 17.02.2018
 */

namespace repositories;

use DB;
use db1\Owner;

class OwnerRepository
{
    public function getModel()
    {
        return new Owner();
    }
}