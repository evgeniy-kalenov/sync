<?php
/**
 * User: Evgeniy Kalenov
 * Date: 19.02.2018
 */

namespace repositories;

use DB;
use db1\DB1;
use db2\DB2;

abstract class Repository
{
    protected static $connect;
    protected static $db_name;

    /*
     * Создаем соединение с БД
     * @param string
     */
    public function __construct($db_name)
    {
        if($db_name == DB::DB1_NAME){
            self::$db_name = $db_name;
            self::$connect = new DB1(self::$db_name);
        }
        elseif ($db_name == DB::DB2_NAME){
            self::$db_name = $db_name;
            self::$connect = new DB2(self::$db_name);
        }
    }

    abstract function getModel();

    /*
     * Возвращает все записи
     * @return array
     */
    public function find()
    {
        $model = $this->getModel();
        $table = $model->getTable();

        $select = "SELECT * FROM $table";
        $data = self::$connect->query($select);

        $res = [];
        $i = 0;
        while($row = $data->fetchArray(SQLITE3_ASSOC)){
            if(!isset($row['id'])){
                continue;
            }
            $model = $this->getModel();
            $model->setAttributes($row);

            $res[$i] = $model;
            $i++;
        }
        return $res;
    }

    /*
     * Создает запись объекта в БД
     * @return boolean
     */
    public function create($model = null)
    {
        if(!$model){
            return;
        }
        $table = $model->getTable();

        $fields = implode(', ', $model->getProperties());
        $values = implode(', ', $model->getBindProperties());
        $insert = "INSERT INTO $table ($fields) VALUES ($values)";
        $stmt = self::$connect->prepare($insert);

        $bind_params = $model->getBindParams();
        foreach ($bind_params as $param => $value){
            $stmt->bindParam($param, $bind_params[$param]);
        }

        $res = $stmt->execute();
        return $res->finalize();
    }

    /*
     * Создает/сохраняет объект в БД
     * @return integer | boolean
     */
    public function save($model = null)
    {
        if(!$model){
            return;
        }
        $table = $model->getTable();

        $params = $model->getBindParamsForUpdate();
        $model->modifiedAt = date("Y-m-d H:i:s");

        $update = "UPDATE $table SET $params WHERE id = :id";
        $stmt = self::$connect->prepare($update);

        $bind_params = $model->getBindParams();
        foreach ($bind_params as $param => $value){
            $stmt->bindParam($param, $bind_params[$param]);
        }

        $res = $stmt->execute();
        return $res->finalize();
    }

    /*
     * Удаляет объект из БД
     * @return boolean
     */
    public static function delete($model = null)
    {
        if(!$model){
            return;
        }
        $table = $model->getTable();

        $delete = "DELETE FROM $table WHERE id = :id";
        $stmt = self::$connect->prepare($delete);
        $stmt->bindParam(':id', $model->id);

        $res = $stmt->execute();
        return $res->finalize();
    }
}