<?php
/**
 * Created by PhpStorm.
 * User: Kalenov Evgeniy
 * Date: 17.02.2018
 */

namespace repositories;

use DB;

class SkuRepository extends Repository
{
    public function getModel()
    {
        switch (self::$db_name){
            case DB::DB1_NAME:
                return new \db1\Sku();
                break;
            case DB::DB2_NAME:
                return new \db2\Sku();
                break;
            default:
                throw new \Exception("Не определен объект репозитория");
        }
    }
}