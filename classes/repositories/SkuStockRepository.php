<?php
/**
 * Created by PhpStorm.
 * User: Kalenov Evgeniy
 * Date: 17.02.2018
 */

namespace repositories;

use DB;
use db2\SkuStock;

class SkuStockRepository extends Repository
{
    public function getModel()
    {
        return new SkuStock();
    }
}