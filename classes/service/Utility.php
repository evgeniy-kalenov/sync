<?php
/**
 * User: Evgeniy Kalenov
 * Date: 16.02.2018
 */

namespace service;

class Utility
{
    /*
     * @param mixed
     */
    public static function debug($data, $format = false)
    {
        echo '<pre>';
        $format ? var_dump($data) : print_r($data);
        echo '</pre>';
    }
}