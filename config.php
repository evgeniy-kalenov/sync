<?php
/**
 * User: Evgeniy Kalenov
 * Date: 16.02.2018
 */

function __autoload($class_name){
    include "classes/$class_name.php";
}

const DB_PATH = __DIR__ . '\\databases';
const FILES_PATH = __DIR__ . '\\files';