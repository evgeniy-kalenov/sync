/*Table structure for table `Employee` */

DROP TABLE IF EXISTS `Employee`;

CREATE TABLE `Employee` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `createAt` DATETIME NOT NULL,
  `modifiedAt` DATETIME DEFAULT NULL,
  `name` TEXT NOT NULL
);

/*Data for the table `Employee` */


/*Table structure for table `Outlet` */

DROP TABLE IF EXISTS `Outlet`;

CREATE TABLE `Outlet` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `createAt` DATETIME NOT NULL,
  `modifiedAt` DATETIME DEFAULT NULL,
  `name` TEXT NOT NULL,
  `owner_name` TEXT DEFAULT NULL
);

/*Data for the table `Outlet` */


/*Table structure for table `Sku` */

DROP TABLE IF EXISTS `Sku`;

CREATE TABLE `Sku` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `createAt` DATETIME NOT NULL,
  `modifiedAt` DATETIME DEFAULT NULL,
  `name` TEXT NOT NULL
);

/*Data for the table `SkuStock` */

DROP TABLE IF EXISTS `SkuStock`;

CREATE TABLE `SkuStock` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `createAt` DATETIME NOT NULL,
  `modifiedAt` DATETIME DEFAULT NULL,
  `sku_id` INTEGER NOT NULL,
  `stock` INTEGER DEFAULT NULL
);

/*Data for the table `SkuStock` */

/*Table structure for table `ObjectUnload` */

DROP TABLE IF EXISTS `ObjectUnload`;

CREATE TABLE `Unload` (
  `type` TEXT NOT NULL,
  `id` INTEGER NOT NULL,
  `state_1` TEXT NOT NULL DEFAULT NULL,
  `state_2` TEXT NOT NULL DEFAULT NULL,
  `state_3` TEXT NOT NULL DEFAULT NULL,
  `relation_id` INTEGER DEFAULT NULL
);

/*Data for the table `ObjectUnload` */