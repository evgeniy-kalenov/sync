<?php
/**
 * User: Evgeniy Kalenov
 * Date: 16.02.2018
 */

include 'config.php';

use repositories\EmployeeRepository;
use repositories\OutletRepository;
use repositories\OwnerRepository;
use repositories\SkuRepository;
use repositories\SkuStockRepository;
use service\Utility;
use db1\DB1;
use db2\DB2;

/*
 * Инициализация баз данных из SQL-скрипта (файлы в /files) есть они не существуют (/databases)
 */
DB::init();

/*
 * Объекты EmployeeRepository, OutletRepository, OwnerRepository, SkuRepository, SkuStockRepository достают, сохраняют, изменяют, удаляют соотв. объекты из баз
 */
$rep = new OutletRepository(DB::DB1_NAME);
$res = $rep->find();                                // вернет все записи магазинов из БД1

$rep2 = new OutletRepository(DB::DB2_NAME);
$res2 = $rep2->find();                              // вернет все записи магазинов из БД2

/*
 * Создание объекта - магазин
 */
$model = new \db1\Outlet();
$model->name = 'Ozon';
$rep->create($model);                               // записывает объект в БД1

/*
 * Удаление объекта - магазин
 */
if(!empty($rep2)){
    $model = $res2[0];
    $model->name = 'New name';
    // $rep2->save($model);                            // изменение объекта из БД2   
    // $rep2->delete($model);                          // удаление объекта из БД2   
}

/*
 * Синхронизация БД (не завершено)
 */
$db = new DB1();
$db->synchronize();

?>

<table width="100%">
    <tr>
        <td>База данных 1</td>
        <td>База данных 2</td>
    </tr>
    <tr style="vertical-align: top">
        <td>
            <?php
                Utility::debug($res);
            ?>
        </td>
        <td>
            <?php
                Utility::debug($res2);
            ?>
        </td>
    </tr>
</table>
